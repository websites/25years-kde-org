# 25years.kde.org

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_25years-kde-org)](https://binary-factory.kde.org/job/Website_25years-kde-org/)

This is the git repository for [25years.kde.org](https://25years.kde.org), the website for the KDE's 25th Anniversary.

As a (Hu)Go module, it requires both Hugo and Go to work.

### Development
Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/)

### I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n)
